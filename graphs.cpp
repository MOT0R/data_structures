#include <iostream>
#include <fstream>
#include <stdio.h>
#include <List.h>
#include <queue>

class Graph
{
	void error(char* s, char* s2)
	{
		std::cout << s << ' ' << s2 << '\n';
		exit(1);
	}  
	friend void BFS(Graph &graph, int s, std::queue <int> &q, 
					int* color, int* predecessor, int* d);
	int vertices_amount; 
    List **adj_list;
public:
	int get_vertices_amount() { return vertices_amount; }
	void print_adj(int i) { adj_list[i]->print_list(); }
    ~Graph();
	Graph(char* file_name);
};

Graph::~Graph()
{
	delete [] adj_list;
}

Graph::Graph(char* file_name)
{
	int num_of_vertices;
	int x;
	std::ifstream fd;
	fd.open(file_name);
	if (!fd)
		error("cannot open input file \n", file_name);
	fd >> num_of_vertices;
	adj_list = new List*[num_of_vertices];

	for (int i = 0; i < num_of_vertices; i++)
		adj_list[i] = new List;

	for (int i = 0; i < num_of_vertices; i++)
		for(int j = 0; j < num_of_vertices; j++)
		{
			fd >> x;
			if (x == 1)
				adj_list[i]->push(j);
		}
	fd.close();
}

enum { white, gray, black };

void BFS(Graph &graph, int s, std::queue <int> &q, int* color, int* predecessor, int* d)
{
	for(int i = 0; i < graph.get_vertices_amount(); i++)
	{
		color[i] = white;
		d[i] = -1;
		predecessor[i] = -1;
	}
	color[s] = gray;
	d[s] = 0;
	predecessor[s] = -1;
	q.push(s);
	while (!q.empty())
	{
		int u = q.front();
		q.pop();
		Node* tmp = graph.adj_list[u]->get_start();
		while(tmp)
		{
			if (color[tmp->val] == white)
			{
				color[tmp->val] = gray;
				d[tmp->val]++;
				predecessor[tmp->val] = u;
				q.push(tmp->val);
			}
			tmp = tmp->next;
		}
		color[u] = black;
	}
}


int main(int argc, char* argv[]) // I need to read again about it
{
	Graph A(argv[1]);
	A.print_adj(0);
	A.print_adj(1);
	A.print_adj(2);
	A.print_adj(3);
    return 0;
}
