struct Node
{
	int val;
	Node* next;
	Node(int x = 0, Node* n_next = 0) : val(x), next(n_next) {}
};

class List
{
	Node* start;
public:
	List() : start(0) {}
	void push(int x);
	void print_list();
	Node* get_start() { return start; } 
	~List();
};
