#include <iostream>
#include <List.h>

void List::push(int x)
{
	Node* tmp = start;
	if(!tmp)
		start = new Node(x);
	else
	{
		while(tmp->next) 
			tmp = tmp->next;
		tmp->next = new Node(x);
	}
}

void List::print_list()
{
	Node* tmp = start;
	while(tmp)
	{
		std::cout << tmp->val << "\n";
		tmp = tmp->next;
	}
}

List::~List()
{
	Node* tmp;
	while(start)
	{
		tmp = start->next;
		delete start;
		start = tmp;
	}
}
